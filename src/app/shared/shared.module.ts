import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { CardPeliculaComponent } from './components/card-pelicula/card-pelicula.component';
import { PeliculasSimilaresComponent } from './components/peliculas-similares/peliculas-similares.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    NavComponent,
    CardPeliculaComponent,
    PeliculasSimilaresComponent
  ],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports: [
    NavComponent,
    PeliculasSimilaresComponent
  ]
})
export class SharedModule { }
